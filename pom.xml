<!--
  ~ PrearcImporter: pom.xml
  ~ XNAT http://www.xnat.org
  ~ Copyright (c) 2019, Washington University School of Medicine
  ~ All Rights Reserved
  ~
  ~ Released under the Simplified BSD.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.nrg</groupId>
        <artifactId>parent</artifactId>
        <version>1.7.6</version>
    </parent>

    <artifactId>PrearcImporter</artifactId>

    <name>PrearcImporter</name>
    <description>Utilities for extracting imaging data from archives, organizing it into sessions, extracting metadata
        (DICOM, ECAT, or Siemens IMA), and translating to XNAT session metadata.
    </description>

    <url>http://www.xnat.org</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <issueManagement>
        <system>XNAT Issue Tracker</system>
        <url>http://issues.xnat.org</url>
    </issueManagement>

    <scm>
        <url>https://bitbucket.org/xnatdev/prearc-importer</url>
    </scm>

    <organization>
        <name>Neuroinformatics Research Group</name>
        <url>http://nrg.wustl.edu</url>
    </organization>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                    <archive>
                        <manifest>
                            <mainClass>org.nrg.PrearcImporter</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>framework</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>*</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>dicom-xnat-mx</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>ecat4xnat</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>dicomtools</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>DicomImageUtils</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.ant</groupId>
            <artifactId>ant</artifactId>
        </dependency>
        <dependency>
            <groupId>dom4j</groupId>
            <artifactId>dom4j</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>org.nrg.maven.artifacts.release</id>
            <name>XNAT Release Maven Repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-release</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.maven.artifacts.snapshot</id>
            <name>xnat snapshot maven repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </snapshots>
        </repository>
    </repositories>

</project>
